import React from 'react';
import './App.css';
import Nbig from './components/Nbig';
import Nsmall from './components/Nsmall';
import Ngrid from './components/Ngrid'



function App() {
  return (
    <div className="App">
     <Nbig/>
     <Nsmall/>
     <Ngrid/>
    </div>
  );
}

export default App;


