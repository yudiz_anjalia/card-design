// import React, { useEffect , useState} from "react";

// function Card() {

//     const [score , setScore] = useState

//     useEffect(() => {
//         const fetchData = async () => {
//             const response = await fetch('https://gateway-dev.crictracker.ml/', {
//                 method: 'POST',
//                 headers: { 'Content-Type': 'application/json' },
//                 body: JSON.stringify({
//                   query: `query GetHomePageArticle($input: getHomePageArticleInput) {
//                     getHomePageArticle(input: $input) {
//                       nTotal
//                       aResults {
//                         eType
//                         sName
//                         iSeriesId
//                         sSlug
//                         bScoreCard
//                         aArticle {
//                           sType
//                             _id
//                             sTitle
//                             sSubtitle
//                             sSrtTitle
//                             oSeo {
//                               sSlug
//                             }
//                             sDescription
//                             oImg {
//                               sUrl
//                               sText
//                               sCaption
//                               sAttribute
//                             }
//                             nDuration
//                             dPublishDate
//                             oCategory {
//                               sName
//                               _id
//                               oSeo {
//                                 sSlug
//                               }
//                             }
//                             oTImg {
//                               sUrl
//                               sText
//                               sCaption
//                               sAttribute
//                             }
//                         }
//                       }
//                     }
//                   }`,
//                   variables: { input: { nSkip: 1, nLimit: 50 } }
//                 })
//               })
//              const data = await response.json()
//              setScore(data)
//              console.log("data", data.data.getHomePageArticle.aResults[0].sName);
//              return(data)
//         }
//         fetchData();
//     }, []);

//     // function test() {
//     //     const test1 = 'asd'
//     //     console.log(test1)
//     // }
//     // console.log(test1)
//     console.log(score);
//     return (
        
//         <div>
//         {/* <h1>{data.getHomePageArticle.aResults[0].sName}</h1> */}
//         {/* <h1 >{data.data.getHomePageArticle}</h1> */}
//         </div>
//     )
// }

    
  
// export default Card

export default async function graphQlToRest() {
    const response = await fetch("https://gateway-dev.crictracker.ml/", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `query GetHomePageArticle($input: getHomePageArticleInput) {
          getHomePageArticle(input: $input) {
            nTotal
            aResults {
              eType
              sName
              iSeriesId
              sSlug
              bScoreCard
              aArticle {
                sType
                  _id
                  sTitle
                  sSubtitle
                  sSrtTitle
                  oSeo {
                    sSlug
                  }
                  sDescription
                  oImg {
                    sUrl
                    sText
                    sCaption
                    sAttribute
                  }
                  nDuration
                  dPublishDate
                  oCategory {
                    sName
                    _id
                    oSeo {
                      sSlug
                    }
                  }
                  oTImg {
                    sUrl
                    sText
                    sCaption
                    sAttribute
                  }
              }
            }
          }
        }`,
        variables: { input: { nSkip: 1, nLimit: 50 } },
      }),
    });
    const data = await response.json();
    return data;
  }
  