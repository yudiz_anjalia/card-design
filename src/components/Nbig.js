import React, { useEffect, useState } from "react";
import graphQlToRest from "./Carddata";
import './title.css'

function Nbig() {
  const [cardData, setCardData] = useState([]);
  const [tile, setTile] = useState([]);
  const [desc , setDesc] = useState([]);

  useEffect(() => {
    graphQlToRest()
      .then((cardData) =>
        setCardData(cardData.data.getHomePageArticle.aResults[0].sName)
  
      )
  }, []);
// console.log(cardData)

useEffect(() => {
    graphQlToRest()
      .then((tile) =>
        setTile(tile.data.getHomePageArticle.aResults[0].aArticle[0].sTitle)
      )
  }, []);
//   console.log(tile)

useEffect(() => {
    graphQlToRest()
      .then((desc) =>
        setDesc(desc.data.getHomePageArticle.aResults[0].aArticle[0].sDescription)
      )
  }, []);



  return (
    <div className="main">
      <div className="main1">
          <div className="card">
        <h1>{cardData}</h1>
        <hr className="hr" />
        <img className="img" src="https://wallpaperaccess.com/full/1869019.jpg"></img>
        <h4 style={{ textAlign: "left" }}>{tile}</h4>
        <small style={{ textAlign: "left" }} >{desc}</small>
        </div>
        
      </div>
    </div>
  );
}

export default Nbig;