import React, { useEffect, useState } from "react";
import graphQlToRest from "./Carddata";
import './small.css'

function Nsmall() {
  const [title, setTitle] = useState([]);
 const [title1, setTitle1] = useState([]);

  useEffect(() => {
    graphQlToRest()
      .then((title) =>
        setTitle(title.data.getHomePageArticle.aResults[0].aArticle[1].sTitle)
      )
  }, []);
// console.log(title)

useEffect(() => {
    graphQlToRest()
      .then((title1) =>
        setTitle1(title1.data.getHomePageArticle.aResults[0].aArticle[2].sTitle)
      )
  }, []);
// console.log(title1)




  return (
    <div className="main">
        <div className="section">
      <div className="card1" >
        <div><img className="img1" src="https://wallpaperaccess.com/full/1869019.jpg"></img></div>
        <div className="content">
            <small >{title}</small>
        </div>
      </div>
      
      <br/>
      <div className="card2">
          <br/>
      <div><img className="img1" src="https://wallpaperaccess.com/full/1869019.jpg"></img></div>
        <div className="content">
            <small >{title1}</small>
        </div>
      </div>
      </div>
    </div>
  );
}

export default Nsmall;